/**
 * Copyright (c) 2015-2016, Michael Yang 杨福海 (fuhai999@gmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.jpress.cache.impl;

import com.jfinal.plugin.ehcache.IDataLoader;
import io.jpress.cache.ICache;
import java.util.List;
import net.oschina.j2cache.CacheChannel;
import net.oschina.j2cache.CacheObject;

public class J2Cache implements ICache {
    static CacheChannel cache;

    static {
        System.setProperty("java.net.preferIPv4Stack", "true"); //Disable IPv6 in JVM
        cache = net.oschina.j2cache.J2Cache.getChannel();
    }

    @Override
    public <T> T get(String cacheName, Object key) {
        CacheObject object = cache.get(cacheName, key);
        return (T) object.getValue();
    }

    @Override
    public void put(String cacheName, Object key, Object value) {
        cache.set(cacheName, key, value);
    }

    @Override
    public List<?> getKeys(String cacheName) {
        return cache.keys(cacheName);
    }

    @Override
    public void remove(String cacheName, Object key) {
        cache.evict(cacheName, key);
    }

    @Override
    public void removeAll(String cacheName) {
        cache.clear(cacheName);
    }

    @Override
    public <T> T get(String cacheName, Object key, IDataLoader dataLoader) {
        Object data = get(cacheName, key);
        if (data == null) {
            data = dataLoader.load();
            put(cacheName, key, data);
        }
        return (T)data;
    }
}
