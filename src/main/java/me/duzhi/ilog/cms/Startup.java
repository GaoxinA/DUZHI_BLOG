package me.duzhi.ilog.cms;

import com.jfinal.core.JFinalFilter;
import com.jfinal.log.Log;
import com.jfinal.server.JettyServerForIDEA;
import com.sun.tools.javac.util.ArrayUtils;
import io.jpress.utils.StringUtils;
import me.duzhi.ilog.cms.function.Functions;
import org.eclipse.jetty.server.*;
import org.eclipse.jetty.server.handler.ContextHandlerCollection;
import org.eclipse.jetty.server.nio.NetworkTrafficSelectChannelConnector;
import org.eclipse.jetty.servlet.FilterHolder;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.util.Jetty;
import org.eclipse.jetty.webapp.Configuration;
import org.eclipse.jetty.webapp.WebAppContext;

import javax.servlet.DispatcherType;
import java.util.EnumSet;

/**
 * Copyright (c) 二月,28,2017 (ashang.peng@aliyun.com)
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

public class Startup {
    public static final Log log = Log.getLog(Startup.class);

    public static void main(String[] args) throws Exception {
        Thread.currentThread().setName(args[1] + "_" + args[0]);
        log.error("\n------- ILogCMS Start Up -----------\n" +
                "-- Version : V0.1            -----------\n" +
                "-- Base on :Jress 0.8        -----------\n" +
                "-- Use Jinal .etc            -----------\n" +
                "-- Port :" + args[0] + "       -----------\n");
        final Server server = new Server(Integer.valueOf(args[0]));
        try {
            WebAppContext context = new WebAppContext(args[2], "/");
            context.setParentLoaderPriority(true);
            String requestLogPath = Functions.Kit.get("requestLog");
            if (StringUtils.isNotBlank(requestLogPath)) {
                NCSARequestLog requestLog = new AsyncNCSARequestLog(requestLogPath + args[1] + "-" + args[0] + "-yyyy_mm_dd.request.log");
                requestLog.setAppend(true);
                requestLog.setExtended(false);
                requestLog.setPreferProxiedForAddress(true);
                requestLog.setLogTimeZone("GMT");
                requestLog.setLogLatency(true);
                requestLog.setRetainDays(90);
                requestLog.setLogServer(true);
                requestLog.setExtended(true);
                server.setRequestLog(requestLog);
            }
            server.setHandler(context);
            server.start();
            server.join();
        } catch (
                Exception e)

        {
            e.printStackTrace();
            server.dumpStdErr();
        }
    }
}